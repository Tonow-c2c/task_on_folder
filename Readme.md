## To use

### Differant task
`-t [task]`

* to add one git hook use `flake8` argument

* to know odoo version use `odoo-V`


### Differant directory achitecture
`-d [tree type]`

* `customer`

```
    Check before commit on subdirectory end with :
    'odoo' or 'openerp'
    Like this tree:
    .
    ├── black_resonance
    │   └── black_resonance_odoo
    ├── Dump
    │   ├── upgraded_winter_sunset_3601-20180109-030551.pg.zip
    │   └── winter_sunset_3601-20180620-030919.pg
    ├── frosty_sky
    │   ├── frosty_sky_odoo
    │   ├── log
    │   ├── screenshoot
    │   └── Venvfrosty_sky
    ├── patient_glitter
    │   ├── patient_glitter_odoo
    │   └── screenshoot
    ├── winter_sunset
    │   ├── winter_sunset_openerp
    │   └── screenshoot
    └── flake8_commit.py
```

* `simple`

```
    Like this tree:
    .
    ├── dir1
    ├── dir2
    ├── dir3
    └── dir4
```

### Location argument
`-l [path]`

## Sample

* add flake8 on customer repo
`python3 task_on_multi_dir.py -d customer -t flake8 -l ~/Work/[customers_dir_path]`
* add pdbpp on customer repo
`python3 task_on_multi_dir.py -d customer -t pdbpp -l ~/Work/customers`
* remove all screenshoot
`python3 task_on_multi_dir.py -d customer -t rmallscreenshoot -l ~/Work/customers`
* Install pre commit
`python3 task_on_multi_dir.py -d customer -t pre_commit_init -l ~/Work/customers`
* Switch the src repos to autoshare to save place
`python3 task_on_multi_dir.py -d customer -t switch_src_to_autoshare -l ~/Work/customers`

## Possibility

Commende|Usage
--------|-------
`flake8`| `add git_hook to check flake8`
`git_fresh`| `git fetch pull after asked branch name`
`odoo-V`| `show current odoo version`
`pdbpp`| `add pdbpp to repos without dev_requirement.txt and run doco build`
`pre_commit_init`| `Install pre commit`
`switch_src_to_autoshare`| `Switch the src repos to autoshare`
`test`| `personnal test`

# Check oca dependancy

* got to your odoo repos
* run like `check_oca_dependancy odoo/external-src/account-financial-tools`

## NB

* Use alias like :
    * `alias task_on_multi_dir='python3 ~/PATH/Traitement-dossier/task_on_multi_dir.py'`
    * `alias check_external_dependancy='python3 ~/PATH/Traitement-dossier/check_external_dependancy.py'`
