import getopt
import os
import subprocess
import sys
from file_tasks import check_dev_requirements_exist
from folder_tasks import rm_screenshoots, pre_commit_install, autoshare_src

def do_to_customer_tree(task):
    """
    ############################################################
    Integrate function_to_call
    Check before commit on subdirectory end with :
    'odoo' or 'openerp'
    Like this tree:
    .
    ├── black_resonance
    │   └── black_resonance_odoo
    ├── Dump
    │   ├── upgraded_winter_sunset_3601-20180109-030551.pg.zip
    │   └── winter_sunset_3601-20180620-030919.pg
    ├── frosty_sky
    │   ├── frosty_sky_odoo
    │   ├── log
    │   ├── screenshoot
    │   └── Venvfrosty_sky
    ├── patient_glitter
    │   ├── patient_glitter_odoo
    │   └── screenshoot
    ├── winter_sunset
    │   ├── winter_sunset_openerp
    │   └── screenshoot
    └── flake8_commit.py
    ############################################################ \n
    """
    # print(do_to_customer_tree.__doc__)
    for dir in os.listdir():
        try:
            os.chdir(dir)
            for dir_odoo in os.listdir():
                if dir_odoo.endswith('odoo') or dir_odoo.endswith('openerp'):
                    try:
                        os.chdir(dir_odoo)
                        print(dir_odoo)
                        globals()[task]()
                        os.chdir('..')
                    except Exception as exep_cutomer_tree:
                        print(exep_cutomer_tree)
                        print(os.listdir())
                        pass

            os.chdir('..')
        except Exception as exep1:
            # print(exep1)
            pass


def simple_tree(task):
    """
    ############################################################
    Integrate function_to_call one directory :
    Like this tree:
    .
    ├── dir1
    ├── dir2
    ├── dir3
    └── dir4
    ############################################################ \n
    """
    print(simple_tree.__doc__)

    for dir in os.listdir():
        try:
            os.chdir(dir)
            print(dir)
            globals()[task]()
            os.chdir('..')
        except Exception as exep_simple_tree:
            print(exep_simple_tree)
            pass


def git_hook_flake8():
    """
    Doc here:
    http://flake8.pycqa.org/en/latest/user/using-hooks.html

    """
    # print(git_hook_flake8.__doc__)
    try:
        subprocess.run('flake8 --install-hook git', shell=True, stdout=subprocess.PIPE)
        subprocess.run('git config --bool flake8.strict true', shell=True)
        print("#"*5 + ' ok ' + ";-)" + '\n')
    except Exception as exep_flake8:
        print("-"*5 + 'Nok ! \n{}'.format(exep_flake8))


def git_fetch_pull():
    """
    chose branche
    fetch --all
    pull

    """
    print(git_fetch_pull.__doc__)
    try:
        subprocess.run('git branch -a', shell=True)
        branch = input('Name of checkout branch : ')
        subprocess.run('git checkout ' + branch , shell=True)
        subprocess.run('git fetch', shell=True)
        subprocess.run('git pull', shell=True)
        print("#"*5 + ' ok ' + ";-)" + '\n')
    except Exception as exep_git_fetch_pull:
        print("-"*5 + 'Nok ! \n{}'.format(exep_git_fetch_pull))


def test_task():
    """
    To test task
    """
    print('In {} we have {}\n'.format(os.getcwd(), os.listdir()))


def show_odoo_version():
    try:
        subprocess.run('cat odoo/VERSION', shell=True)
        print('\n')
    except Exception as exep_git_fetch_pull:
        print("Not Docker instance")

def add_pdbpp():
    try:
        check_dev_requirements_exist()
        print("#"*5 + ' ok ' + ";-)" + '\n')
    except Exception as exep:
        print("-"*5 + 'Nok ! \n{}'.format(exep))

def rm_all_screenshoot():
    try:
        rm_screenshoots()
    except Exception as exep:
        print("-"*5 + 'Nok ! \n{}'.format(exep))

def pre_commit_init():
    try:
        pre_commit_install()
    except Exception as exep:
        print("-"*5 + 'Nok ! \n{}'.format(exep))

def switch_src_to_autoshare():
    try:
        autoshare_src()
    except Exception as exep:
        print("-"*5 + 'Nok ! \n{}'.format(exep))



def go_to_right_dir_to_execute(dir):
    """
    Go to directory on parameter if is exist
    """
    print("from : {} to : {}".format(os.getcwd(), dir))
    try:
        os.chdir(dir)
    except Exception as exep_simple_tree:
        print(exep_simple_tree)
        pass


def convert_type_dir(type_dir):
    dict_type = {
        'customer': 'do_to_customer_tree',
        'simple': 'simple_tree'
    }
    if type_dir not in dict_type:
        print('-d <type_dir> {} is invalide list is :'.format(type_dir))
        for key in dict_type:
            print('\t - {}'.format(key))
        sys.exit()
    return dict_type[type_dir]


def convert_type_task(task):
    dict_type = {
        'flake8': 'git_hook_flake8',
        'git_fresh': 'git_fetch_pull',
        'odoo-V': 'show_odoo_version',
        'pdbpp': 'add_pdbpp',
        'rmallscreenshoot': 'rm_all_screenshoot',
        'pre_commit_init': 'pre_commit_init',
        'switch_src_to_autoshare': 'switch_src_to_autoshare',
        'test': 'test_task'
    }
    if task not in dict_type:
        print('-t <task> {} is invalide list is :'.format(task))
        for key in dict_type:
            print('\t - {}'.format(key))
        sys.exit()
    return dict_type[task]


def main():
    """
    To do some task on multi folder
    To run $ pyhton3 task_on_multi_dir.py -d <type_dir> -t <task> -l <locate>

    param : type_dir -> tree type to run
    param : task -> task to do on directory
    param : locate -> where shoule begine the script
    ##########################################################################
    ##########################################################################
    """
    try:
        opts, args = getopt.getopt(sys.argv[1:], "d:t:l:")
    except getopt.GetoptError as erroropts:
        print(main.__doc__)
        print(erroropts)
        sys.exit(2)

    if not opts:
        print(main.__doc__)
        sys.exit()

    for opt, arg in opts:
        if opt == '-h':
            print(main.__doc__)
            sys.exit()
        elif opt in ("-d"):
            type_dir = arg
            move_function = convert_type_dir(type_dir)
        elif opt in ("-t"):
            task = arg
            task_to_do = convert_type_task(task)
        elif opt in ('-l'):
            locate = arg
            print(locate)
            print("go to fonction")
            go_to_right_dir_to_execute(locate)
    globals()[move_function](task_to_do)


if __name__ == "__main__":
    main()
