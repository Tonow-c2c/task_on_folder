import os
import subprocess

def _create_dev_requirements_pdbpp():
    with open('dev_requirements.txt', 'w') as f:
        f.write('pdbpp')

def check_dev_requirements_exist():
    fname = 'dev_requirements.txt'
    try:
        # remove in the old place
        os.remove(fname)
    except Exception as e:
        print('Any {} in {}'.format(fname, os.getcwd()))

    # Add new odoo/dev_requirements.txt
    os.chdir('odoo/')
    if not os.path.isfile(fname):
        _create_dev_requirements_pdbpp()
        print("doco build --pull start")
        os.chdir('..')
        # Run docker-compose build
        subprocess.run('docker-compose build --pull', shell=True, stdout=subprocess.PIPE)
        print("finish" + '\n')
    else:
        # In any case go back to previous directory
        os.chdir('..')
