import os
import shutil
import subprocess

def rm_screenshoots():
    """ Remove all file in screenshoot folder """
    prv_dir = os.getcwd()
    try:
        os.chdir('../screenshoot')
        files = os.listdir()
        size_folder = 0
        for file in files:
            statinfo = os.stat(file)
            size_file = statinfo.st_size
            size_folder += size_file
            print(f'rm : {file} -- size {sizeof_fmt(size_file)}')
            os.remove(file)
        if size_folder:
            print(f'save {sizeof_fmt(size_folder)}')
        os.chdir(prv_dir)
    except Exception as e:
        print(e)

def sizeof_fmt(num, suffix='B'):
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

def pre_commit_install():
    subprocess.run('pre-commit install', shell=True, stdout=subprocess.PIPE)

def autoshare_src():
    # # TODO: check if this work well with oca/ocb
    # github_path_line = False
    # with open('.gitmodules') as f:
    #     for line in f:
    #         if github_path_line:
    #             # '\turl = git@github.com:OCA/OCB.git\n' -> 'OCA/OCB'
    #             github_path = line.split()[-1].split(':')[-1][:-4]
    #             break
    #         if 'path = odoo/src' in line:
    #             # line before the url
    #             github_path_line = True
    # print(github_path)
    autoshare_submodule('odoo/src', 'odoo')

def autoshare_submodule(module_path, autoshare_path):
    subprocess.run(
        f'sudo git submodule deinit -f -- {module_path}',
        shell=True, stdout=subprocess.PIPE
    )
    shutil.rmtree(module_path)
    shutil.rmtree('.git/modules/' + module_path)
    os.makedirs(module_path)
    subprocess.run(f'rm -rf {module_path}', shell=True, stdout=subprocess.PIPE)
    subprocess.run(f'git submodule update --init --reference ~/.cache/git-autoshare/github.com/{autoshare_path} {module_path}', shell=True, stdout=subprocess.PIPE)
