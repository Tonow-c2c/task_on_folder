#!/usr/bin/python
import getopt
import os
import subprocess
import sys
import yaml

def check_external_dependancy(dir):
    try:
        os.chdir(dir)
        migration_content = read_migration_file('../../migration.yml')
    except Exception as exep_simple_tree:
        print(exep_simple_tree)
        pass
    print("\nModule install :\n")
    for mod in os.listdir():
        if mod in migration_content:
            print("\t- " + mod)



def read_migration_file(migration_file):
    with open(migration_file, 'r') as stream:
        content = yaml.load(stream, Loader=yaml.FullLoader)
    migration_content = []
    for version in range(len(content['migration']['versions'])):
        try:
            pass
            migration_content += content['migration']['versions'][version]['addons']['upgrade']
        except Exception as e:
            pass
    return migration_content



def main(dir):
    check_external_dependancy(dir)

if __name__ == "__main__":
    dir = sys.argv[1]
    main(dir)
